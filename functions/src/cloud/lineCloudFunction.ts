/**
 * @copyright 2019
 * @author Parnrawee.P
 */
import * as functions from 'firebase-functions'
import * as requestIp from 'request-ip'
import * as admin from 'firebase-admin'
import {ReplyMessageHandlerFactoryHandler, Handler} from '../factory/internalModule'

export const lineCloudFunction = async (request: functions.https.Request, response: functions.Response) => {

    const clientIp = requestIp.getClientIp(request)
    console.log(`>> Header : ${JSON.stringify(request.headers)}`)
    console.log(`>> Body : ${JSON.stringify(request.body)}`)

    try {

        await ReplyMessageHandlerFactoryHandler.getHandler(Handler.ECHO_STICKER_HANDLER).replyMessage(request.body)

        const timestamp = new Date().toISOString()
        const usertId :string = request.body.events[0].source.userId
        const requestData = { userId: request.body.events[0].source.userId,ip: clientIp, count: 0, updatedAt: timestamp }
        await admin
            .firestore()
            .collection('users')
            .doc(usertId)
            .set(requestData)

        response.status(200).send('HTTP Done');
    } catch (e) {
        console.log(e)
        response.status(500).send(e);
    }
}