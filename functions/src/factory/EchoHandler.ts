import { ReplyMessageHandlerFactoryHandler } from './internalModule'
import { AxiosResponse } from 'axios'

export class EchoHandler extends ReplyMessageHandlerFactoryHandler<Promise<AxiosResponse>> {

    constructor() {
        super()
    }

    public replyMessage(body: any): Promise<Promise<AxiosResponse>> {
        const replyBody = JSON.stringify({
            replyToken: body.events[0].replyToken,
            messages: [
                {
                    type: `text`,
                    text: body.events[0].message.text
                }
            ]
        })

        return this.getLineClient().post('/reply', replyBody)
    }
}