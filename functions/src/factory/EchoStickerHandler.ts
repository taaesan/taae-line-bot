import { ReplyMessageHandlerFactoryHandler } from './internalModule'
import { AxiosResponse } from 'axios'
import { LineStickerSet } from './LineStickerSet';

export class EchoStickerHandler extends ReplyMessageHandlerFactoryHandler<Promise<AxiosResponse>> {

    private _stickerSet: LineStickerSet

    constructor() {
        super()
        this._stickerSet = new LineStickerSet()
        this._stickerSet.name = 'Brown, Cony & Sally'
        this._stickerSet.packageId = 11537
        this._stickerSet.stickerIds = [52002734, 52002735, 52002736, 52002737, 52002738, 52002739, 52002740, 52002741, 52002742, 52002743, 52002744]
    }

    public replyMessage(body: any): Promise<Promise<AxiosResponse>> {
        let replyBody = {}
        if (body.events[0].message.type === 'text') {
            replyBody = JSON.stringify({
                replyToken: body.events[0].replyToken,
                messages: [
                    {
                        type: `text`,
                        text: body.events[0].message.text
                    }
                ]
            })
        } else {
            let length = this._stickerSet.stickerIds.length
            let randomIndex = Math.floor((Math.random() * 10) + 1) % length
            console.log(`randomIndex: ${randomIndex}`)

            replyBody = JSON.stringify({
                replyToken: body.events[0].replyToken,
                messages: [
                    {
                        type: `sticker`,
                        packageId: this._stickerSet.packageId,
                        stickerId: this._stickerSet.stickerIds[randomIndex]
                    }
                ]
            })
        }

        return this.getLineClient().post('/reply', replyBody)
    }
}