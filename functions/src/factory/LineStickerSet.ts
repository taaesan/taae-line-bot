export class LineStickerSet {
    private _name!: string
    private _packageId!: number
    private _stickerIds!: Array<number>

    set name(name: string) {
        this._name = name
    }
    get name(): string {
        return this._name
    }

    set packageId(packageId: number) {
        this._packageId = packageId
    }
    get packageId(): number {
        return this._packageId
    }

    set stickerIds(stickerIds: Array<number>) {
        this._stickerIds = stickerIds
    }
    get stickerIds(): Array<number> {
        return this._stickerIds
    }
}