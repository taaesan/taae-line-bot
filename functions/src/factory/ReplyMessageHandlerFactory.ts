/**
 * @copyright 2019
 * @author Parnrawee.P
 */
import axios, { AxiosResponse } from 'axios'
import { EchoHandler, EchoStickerHandler } from './internalModule'

console.log('process.env.FIREBASE_URL > ' + process.env.FIREBASE_URL)

const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message'
const LINE_HEADER = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${process.env.LINE_CHANNEL_ACCESS_TOKEN}`
}

const lineClient = axios.create({
    baseURL: LINE_MESSAGING_API,
    timeout: 3000,
    headers: LINE_HEADER
})

export enum Handler {
    ECHO_HANDLER,
    ECHO_STICKER_HANDLER
}

export abstract class ReplyMessageHandlerFactoryHandler<T> {

    public abstract replyMessage(body: any): Promise<T>
    public getLineClient() {
        return lineClient
    }

    public static getHandler(handler: Handler): ReplyMessageHandlerFactoryHandler<Promise<AxiosResponse>> {
        switch (handler) {
            case Handler.ECHO_HANDLER:
                return new EchoHandler()
            case Handler.ECHO_STICKER_HANDLER:
                return new EchoStickerHandler()
        }
    }
}

