
/**
 * @copyright 2019
 * @author Parnrawee.P
 */
console.log('dotenv: ', require('dotenv').config())
import * as functions from 'firebase-functions'
import { lineCloudFunction } from './cloud/lineCloudFunction'
import * as admin from 'firebase-admin'

const serviceAccount = require('../serviceAccount.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_URL
})

const firestore = admin.firestore()
firestore.settings({ timestampsInSnapshots: true })

export const lineHandler = functions
  .region('asia-northeast1')
  .https
  .onRequest(lineCloudFunction)


